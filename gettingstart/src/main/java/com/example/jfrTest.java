package com.example;

import jnr.ffi.LibraryLoader;

/**
 * Hello world!
 *
 */
public class jfrTest {
    public interface HeapTestLib {
        void func01();
        
        void func02();
        
        void func03();
        
        void func04();
        
        void func05();
        
        void func06();
        
        void func07();
        
        void func08();

        void func09();
        
        void func10();
        
    }

    public interface HeapTestLib2 {
        void func01();

        void func02();

        void func03();

        void func04();

        void func05();

        void func06();

        void func07();

        void func08();

        void func09();

        void func10();

    }

    public static void main(String[] args) throws InterruptedException {

        Thread.sleep(30000);
        HeapTestLib heapTestLib = LibraryLoader.create(HeapTestLib.class).failImmediately().load("heapTestLib");
        Thread.sleep(10000);

        heapTestLib.func01();
        Thread.sleep(10000);

        heapTestLib.func02();
        Thread.sleep(10000);

        heapTestLib.func03();
        Thread.sleep(10000);

        heapTestLib.func04();
        Thread.sleep(10000);

        heapTestLib.func05();
        Thread.sleep(10000);

        heapTestLib.func06();
        Thread.sleep(10000);

        heapTestLib.func07();
        Thread.sleep(10000);

        heapTestLib.func08();
        Thread.sleep(10000);

        heapTestLib.func09();
        Thread.sleep(10000);

        heapTestLib.func10();
        Thread.sleep(10000);

        HeapTestLib2 heapTestLib2 = LibraryLoader.create(HeapTestLib2.class).failImmediately().load("heapTestLib2");
        Thread.sleep(10000);


        heapTestLib2.func01();
        Thread.sleep(10000);
        
        heapTestLib2.func02();
        Thread.sleep(10000);

        heapTestLib2.func03();
        Thread.sleep(10000);

        heapTestLib2.func04();
        Thread.sleep(10000);

        heapTestLib2.func05();
        Thread.sleep(10000);

        heapTestLib2.func06();
        Thread.sleep(10000);

        heapTestLib2.func07();
        Thread.sleep(10000);

        heapTestLib2.func08();
        Thread.sleep(10000);

        heapTestLib2.func09();
        Thread.sleep(10000);

        heapTestLib2.func10();
        Thread.sleep(10000);

    }
}