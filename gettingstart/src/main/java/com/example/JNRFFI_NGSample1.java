package com.example;

import jnr.ffi.LibraryLoader;

/**
 * 関数の定義誤りの例
 *
 */
public class JNRFFI_NGSample1 {
    public interface LibLoadSample {
        boolean func01(int arg01);
    }

    public static void main(String[] args) throws InterruptedException {
        
        // createには作成したインタフェースを指定する
        // loadにはネイティブライブラリのライブラリを指定する
        //    Linuxの場合、libSample.so→libと、soをのぞいた"Sample"を指定する
        LibLoadSample libLoadSample = LibraryLoader.create(LibLoadSample.class).failImmediately().load("loadSample"); 
        //生成した呼び出し用IFで関数を呼び出す
        
        boolean  resultValue = false;
        resultValue = libLoadSample.func01(1);

        if (resultValue){
            System.out.println("呼び出し結果は成功");
        }else{
            System.out.println("呼び出し結果は失敗");
        }        

    }
}