package com.example;

import jnr.ffi.LibraryLoader;

/**
 * Hello world!
 *
 */
public class If02Func01 {
    public interface HeapTestLib {
        void func01();
    }

    public interface HeapTestLib2 {
        void func01();
    }

    public static void main(String[] args) throws InterruptedException {
        HeapTestLib heapTestLib = LibraryLoader.create(HeapTestLib.class).failImmediately().load("heapTestLib"); 
        HeapTestLib2 heapTestLib2 = LibraryLoader.create(HeapTestLib2.class).failImmediately().load("heapTestLib2");
        heapTestLib.func01();
        heapTestLib2.func01();
        
        Thread.sleep(300000);
    }
}