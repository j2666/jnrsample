package com.example;

import jnr.ffi.LibraryLoader;

/**
 * Hello world!
 *
 */
public class If02Func10 {
    public interface HeapTestLib {
        void func01();
        
        void func02();
        
        void func03();
        
        void func04();
        
        void func05();
        
        void func06();
        
        void func07();
        
        void func08();

        void func09();
        
        void func10();
        
    }

    public interface HeapTestLib2 {
        void func01();

        void func02();

        void func03();

        void func04();

        void func05();

        void func06();

        void func07();

        void func08();

        void func09();

        void func10();

    }

    public static void main(String[] args) throws InterruptedException {
        HeapTestLib heapTestLib = LibraryLoader.create(HeapTestLib.class).failImmediately().load("heapTestLib");
        HeapTestLib2 heapTestLib2 = LibraryLoader.create(HeapTestLib2.class).failImmediately().load("heapTestLib2");
        heapTestLib.func01();
        heapTestLib.func02();
        heapTestLib.func03();
        heapTestLib.func04();
        heapTestLib.func05();
        heapTestLib.func06();
        heapTestLib.func07();
        heapTestLib.func08();
        heapTestLib.func09();
        heapTestLib.func10();

        heapTestLib2.func01();
        heapTestLib2.func02();
        heapTestLib2.func03();
        heapTestLib2.func04();
        heapTestLib2.func05();
        heapTestLib2.func06();
        heapTestLib2.func07();
        heapTestLib2.func08();
        heapTestLib2.func09();
        heapTestLib2.func10();

        Thread.sleep(300000);
    }
}