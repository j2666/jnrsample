package com.example;

import jnr.ffi.LibraryLoader;

/**
 * 関数の定義誤り(関数名なし)
 *
 */
public class JNRFFI_NGSample2 {
    public interface LibLoadSample {
        double func01(String arg01);
    }

    public static void main(String[] args) throws InterruptedException {
        
        // createには作成したインタフェースを指定する
        // loadにはネイティブライブラリのライブラリを指定する
        //    Linuxの場合、libSample.so→libと、soをのぞいた"Sample"を指定する
        LibLoadSample libLoadSample = LibraryLoader.create(LibLoadSample.class).failImmediately().load("loadSample"); 
        //生成した呼び出し用IFで関数を呼び出す
        
        double  resultValue = 0;
        resultValue = libLoadSample.func01("Hellow World!");


    }
}