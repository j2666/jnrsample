package com.example;

import jnr.ffi.LibraryLoader;

/**
 * Hello world!
 *
 */
public class If01Func10 {
    public interface HeapTestLib {
        void func01();
        
        void func02();
        
        void func03();
        
        void func04();
        
        void func05();
        
        void func06();
        
        void func07();
        
        void func08();

        void func09();
        
        void func10();
        
    }

    public static void main(String[] args) throws InterruptedException {
        HeapTestLib heapTestLib = LibraryLoader.create(HeapTestLib.class).failImmediately().load("heapTestLib");
        heapTestLib.func01();
        heapTestLib.func02();
        heapTestLib.func03();
        heapTestLib.func04();
        heapTestLib.func05();
        heapTestLib.func06();
        heapTestLib.func07();
        heapTestLib.func08();
        heapTestLib.func09();
        heapTestLib.func10();
        Thread.sleep(300000);
    }
}