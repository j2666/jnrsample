package com.example;

import jnr.ffi.LibraryLoader;

/**
 * Hello world!
 *
 */
public class If01Func01 {
    public interface HeapTestLib {
        void func01();
    }

    public static void main(String[] args) throws InterruptedException {
        HeapTestLib heapTestLib = LibraryLoader.create(HeapTestLib.class).failImmediately().load("heapTestLib"); 
        heapTestLib.func01();
        Thread.sleep(300000);
    }
}